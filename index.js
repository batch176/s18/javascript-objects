function Pokemon(name, level) {
	//properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level

	//methods
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack ));

		target.health -= this.attack

		if(target.health <= 0) {
			this.faint(target)
		}

		};
		this.faint = function() {
		 console.log(this.name + " fainted.")
	}
}

let Bulbasuar = new Pokemon("Bulbasuar", 20);
let Jigglypuff = new Pokemon ("Jigglypuff", 15);